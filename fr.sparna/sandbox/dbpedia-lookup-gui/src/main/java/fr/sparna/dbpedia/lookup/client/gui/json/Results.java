package fr.sparna.dbpedia.lookup.client.gui.json;

import java.util.ArrayList;
import java.util.List;

public class Results {

	private List<Result> results = new ArrayList<Result>();

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}
	
	
}
